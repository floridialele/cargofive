from django import forms
from django.forms import ModelForm
from containers.models import Contract


class ContractForm(forms.ModelForm):
    class Meta:
        model = Contract
        fields = "__all__"
